package test.data_structures;
import junit.framework.TestCase;

import model.data_structures.ListaEnlazada;
import model.data_structures.Node;
public class TestListaEnlazada extends TestCase{

	private Node raiz;
	private int tamanio;
	private ListaEnlazada lista;
	private void setupEscenario1()
	{
		raiz = null;
		tamanio = 0;
		lista = new ListaEnlazada();
	}
	private void setupEscenario2()
	{
		int item1 = 1;
		int item2 = 2;
		int item3 = 3;
		
		lista.agregarAlFinal(item1);
		lista.agregarAlFinal(item2);
		lista.agregarAlFinal(item3);
		
		tamanio =3;
	}
	
	public void testConstructor()
	{
		setupEscenario1();
		assertEquals("Error al inicializar el tama�o de la lista",tamanio,lista.getTamanio());
		assertEquals("Error al inicializar la raiz de la lista", raiz,lista.getNode(0));
	}
	
	public void testEsVacia()
	{
		setupEscenario1();
		assertFalse("La lista deber�a de estar vac�a",lista.esVacia());
		setupEscenario2();
		assertTrue("La lista no deber�a de estar vac�a", lista.esVacia());
	}
	
	public void testGetTamanio()
	{
		setupEscenario1();
		assertEquals("El tama�o deber�a de ser 0",tamanio, lista.getTamanio() );
		setupEscenario2();
		assertEquals("El tama�o deber�a de ser 3",tamanio, lista.getTamanio() );
	}
	public void testInsertar()
	{
		setupEscenario2();
		int itemaux=6;
		lista.insertar(1, itemaux);
		assertEquals("Error al insertar en el medio un item",itemaux,lista.getNode(1));		
	}
	public void testAgregarAlFinal()
	{
		setupEscenario2();
		int itemaux=10;
		lista.agregarAlFinal(itemaux);
		assertEquals("Error al agregar al final un item",itemaux,lista.getNode(tamanio-1));
	}		
	
	public void testSet()
	{
		setupEscenario2();
		int itemaux = 8;
		lista.set(1, itemaux);
		assertEquals("Error al cambiar el item del nodo actual",itemaux,lista.getNode(1));
	}
	public void testEliminar()
	{
		setupEscenario2();
		Node siguienteAEliminar=lista.getNode(2);
		lista.eliminar(1);
		assertEquals("Error al eliminar el nodo seleccionado",siguienteAEliminar,lista.getNode(1));
	}
	
	public void testClear()
	{
		setupEscenario2();
		lista.clear();
		assertFalse("La lista deber�a de estar vac�a",lista.esVacia());
	}
}

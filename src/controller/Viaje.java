package controller;

public class Viaje {
	/**
	 * Origen del viaje
	 */
	private int sourceid;
	/**
	 * Destino del viaje
	 */
	private int dstid;
	private int time;
	private int hod;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;
	private String timeType;

	public Viaje (int pSourceid, int pDstid, int pTime,  double pMean_travel_time, double pStandard_deviation_travel_time,double pGeometric_mean_travel_time, double pGeometric_standard_deviation_travel_time, String pTimeType)
	{
		sourceid = pSourceid;
		dstid = pDstid;
		time = pTime;
//		hod=pHod;
		mean_travel_time = pMean_travel_time;
		standard_deviation_travel_time = pStandard_deviation_travel_time;
		geometric_mean_travel_time = pGeometric_mean_travel_time;
		geometric_standard_deviation_travel_time = pGeometric_standard_deviation_travel_time;
		timeType = pTimeType;
	}
	
	public int darIDOrigen()
	{
		return sourceid;
	}
//	public int darHora(){
//		return hod;
//	}
	public int darIDDestino()
	{
		return dstid;
	}
	public int darHora(){
		int hora=0;
		if(timeType.equals("hod")){
			hora=darTime();
		}
		return hora;
	}
	public int darTime()
	{
		return time;
	}
	
	public double darTiempoPromedioViaje()
	{
		return  mean_travel_time;
	}
	
	public double darDerivacionEstandarViaje()
	{
		return standard_deviation_travel_time;
	}
	
	public double darTiempoPromedioGeometricoViaje()
	{
		return geometric_mean_travel_time;
	}
	
	public double darDerivacionEstandarGeometricaViaje()
	{
		return geometric_standard_deviation_travel_time;
	}
	
	public String darTimeType()
	{
		return timeType;
	}
	
}

package model.data_structures;

public class ListaEnlazada <T> {
	
	Node<T> raiz;
	int tamanio;
	Node<T> ultimo;
	
	public ListaEnlazada()
	{
		raiz = null;
		tamanio = 0;
		ultimo = null;
	}
	
	public Node<T> getNode(int pIndex)
	{
		if(pIndex < tamanio && pIndex >= 0)
		{
			Node<T> node = raiz;
			for(int i = 0;i < pIndex; i++)
			{
				node = node.getNext();
			}
			return node;
		} else {
			
			throw new IndexOutOfBoundsException("El p�rametro introducido no es mayor al tama�o de la lista o tiene valor negativo");
		}
	}
	
	public Node<T> darRaiz()
	{
		return raiz;
	}
	
	public Node<T> darUltimo()
	{
		Node <T> actual = raiz;
		if(actual != null)
			{
			Node <T> sig = actual.getNext();
			while(sig !=null)
			{
				actual = sig;
				sig = actual.getNext();
			}
			
			ultimo = actual;
			}	
		
		
		return ultimo;
	}
	public boolean esVacia()
	{
		return raiz == null;
	}
	
	public int getTamanio()
	{
		return tamanio;
	}
	
	public void insertar(int pIndex, T pItem)
	{
		Node<T> sigNuevo;
		if(pIndex == tamanio)
		{
			sigNuevo = null;
		}
		else
		{
			sigNuevo = getNode(pIndex);
		}
		Node<T> nuevo = new Node <T>(pItem,sigNuevo);
		
		if(pIndex > 0)
		{
			Node <T> antNuevo = getNode(pIndex-1);
			antNuevo.setNext(nuevo);
		} else if( pIndex == 0 || esVacia())
		{
			raiz = nuevo;
		}
		tamanio++;
	}
	
	public void agregarAlFinal (T pItem)
	{
		if(pItem != null)
		{
			Node <T> nuevo = new Node<T>(pItem, null);
			if(raiz == null)
			{
				raiz = nuevo;
				ultimo = nuevo;
			} else {
				ultimo.setNext(nuevo);
				ultimo = nuevo;
			}
			tamanio++;
		}
		
	}
	
	public void set (int pIndex, T pItem)
	{
		Node<T> actual = getNode(pIndex);
		actual.setItem(pItem);
	}
	
	public void eliminar(int pIndex)
	{
		Node<T> aEliminar = getNode(pIndex);
		if(pIndex == 0)
		{
			raiz = aEliminar.getNext();
			aEliminar.setNext(null);
		} 
		else 
		{
			Node<T> antAEliminar = getNode(pIndex-1);
			antAEliminar.setNext(aEliminar.getNext());
		}
		tamanio--;
	}
	
	public void clear()
	{
		raiz = null;
		tamanio = 0;
	}
}

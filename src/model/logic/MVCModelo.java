package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import controller.Viaje;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.ListaEnlazada;
import model.data_structures.Node;
import com.opencsv.CSVReader;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	private final static String ruta1 = "data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv";
	private final static String ruta1D = "data/bogota-cadastral-2018-1-WeeklyAggregate.csv";
	private final static String ruta1H = "data/bogota-cadastral-2018-1-All-HourlyAggregate.csv";
	private final static String ruta2 = "data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv";	
	private final static String ruta2D = "data/bogota-cadastral-2018-2-WeeklyAggregate.csv";
	private final static String ruta2H = "data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";
	private final static String ruta3 = "data/bogota-cadastral-2018-3-All-MonthlyAggregate.csv";
	private final static String ruta3D = "data/bogota-cadastral-2018-3-WeeklyAggregate.csv";
	private final static String ruta3H = "data/bogota-cadastral-2018-3-All-HourlyAggregate.csv";
	private final static String ruta4 = "data/bogota-cadastral-2018-4-All-MonthlyAggregate.csv";
	private final static String ruta4D = "data/bogota-cadastral-2018-4-WeeklyAggregate.csv";
	private final static String ruta4H = "data/bogota-cadastral-2018-4-All-HourlyAggregate.csv";

	/**
	 * Atributos del modelo del mundo
	 */


	public ListaEnlazada<Viaje> listaViajes;


	/**
	 * Constructor del modelo del mundo con capacidad
	 */
	public MVCModelo()
	{
		listaViajes = new ListaEnlazada<> ();
	}

	//Metodos
	//Proyecto 1

	/**
	 * Consultar la informacioÌ�n de los N viajes con mayor tiempo promedio para un mes dado. 
	 * La informacioÌ�n debe mostrarse ordenada de mayor a menor por el tiempo promedio de los viajes. 
	 * @param pCantidadViajes
	 * @param pMes
	 * @return Mostrar los resultados indicando para cada viaje su zona origen, zona destino, el tiempo promedio de viaje y su desviacioÌ�n estaÌ�ndar.
	 */
	public Viaje[] darMayoresViajes(int pCantidadViajes, int pMes){
		Node<Viaje> actual = listaViajes.darRaiz();
		Viaje[] viajes = null;

		while(!(actual == null))
		{
			if(actual.getItem().darTime() == pMes )
			{
				//				viajes++;
			}

			actual = actual.getNext();
		}

		return viajes;
	}

	//Taller1
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamanio()
	{
		return listaViajes.getTamanio();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */


	public ListaEnlazada<Viaje> consultarViajes(int pMes, int pOrigen)
	{
		Node<Viaje> actual = listaViajes.darRaiz();
		ListaEnlazada<Viaje> viajes = new ListaEnlazada();

		while(!(actual == null))
		{
			if(actual.getItem().darTime() == pMes && actual.getItem().darIDOrigen() == pOrigen)
			{
				viajes.agregarAlFinal(actual.getItem());
			}

			actual = actual.getNext();
		}

		return viajes;
	}

	public int consultarNumeroViajesMes(int pMes)
	{
		Node<Viaje> actual = listaViajes.darRaiz();
		int viajes = 0;

		String mes = Integer.toString(pMes);
		if(!(mes.equals("10") || mes.equals("11") || mes.equals("12")) && !(mes.startsWith("0")))
		{
			mes= "0"+ mes;
		} 
		int mesNum = Integer.parseInt(mes);
		while(!(actual == null))
		{
			if(actual.getItem().darTime() == mesNum)
			{
				viajes++;
			}

			actual = actual.getNext();
		}


		return viajes;
	}

	public int consultarNumeroViajesDia(int pDia)
	{
		Node<Viaje> actual = listaViajes.darRaiz();
		int viajes = 0;

		String dia = Integer.toString(pDia);
		if(!(dia.equals("10") || dia.equals("11") || dia.equals("12")) && !(dia.startsWith("0")))
		{
			dia= "0"+ dia;
		} 
		int mesNum = Integer.parseInt(dia);
		while(!(actual == null))
		{
			if(actual.getItem().darTime() == mesNum)
			{
				viajes++;
			}

			actual = actual.getNext();
		}


		return viajes;
	}
	public ArrayList darEstadisticas(int pMes, int pOrigen)
	{
		ArrayList estadisticas = new ArrayList();
		double tamanoLista = listaViajes.getTamanio();
		double tamanioViajesMes = consultarNumeroViajesMes(pMes);
		double porcentaje1 = (100*tamanioViajesMes)/tamanoLista;
		double tamanioViajesMesOrigen = consultarViajes(pMes, pOrigen).getTamanio();
		double porcentaje2 = (100*tamanioViajesMesOrigen)/tamanioViajesMes;

		estadisticas.add(tamanoLista);
		estadisticas.add(tamanioViajesMes);
		estadisticas.add(porcentaje1);
		estadisticas.add(tamanioViajesMesOrigen);
		estadisticas.add(porcentaje2);

		return estadisticas;
	}


	public int cargarViajesALinkedList(String pRuta, String pTiempo)
	{
		BufferedReader reader = null;
		int lineasLeidas=0;

		try {			

			reader = new BufferedReader(new FileReader(pRuta));
			reader.readLine();
			String linea = null;
			while((linea  = reader.readLine()) != null)			
			{
				String[] splitData = linea.split(",");
				if(!(splitData == null)||!(splitData.length == 0))
				{
					Viaje nuevo = new Viaje (Integer.parseInt(splitData[0]),
							Integer.parseInt(splitData[1]), 
							Integer.parseInt(splitData[2]), 
							Double.parseDouble(splitData[3]), 
							Double.parseDouble(splitData[4]), 
							Double.parseDouble(splitData[5]), 
							Double.parseDouble(splitData[6]),
							pTiempo);
					listaViajes.agregarAlFinal(nuevo);
					lineasLeidas++;
				} 				

			}
			reader.close();


		} catch (IOException e)
		{
			e.printStackTrace();
			lineasLeidas--;
		}
		return lineasLeidas;
	}

	public String menorID()
	{
		int menor = listaViajes.darRaiz().getItem().darIDOrigen();
		Node<Viaje> actual = listaViajes.darRaiz();

		while(actual.getNext() != null)
		{
			if(actual.getNext().getItem().darIDOrigen() < menor )
			{
				menor = actual.getNext().getItem().darIDOrigen();
			}
			actual = actual.getNext();
		}
		return Integer.toString(menor);
	}

	public String mayorID()
	{
		int mayor = listaViajes.darRaiz().getItem().darIDOrigen();
		Node<Viaje> actual = listaViajes.darRaiz();

		while(actual.getNext() != null)
		{
			if(actual.getNext().getItem().darIDOrigen() > mayor )
			{
				mayor = actual.getNext().getItem().darIDOrigen();
			}
			actual = actual.getNext();
		}
		return Integer.toString(mayor);
	}
	
	public ArrayList<String> compararViajes(int pMes, int pZonaDada, int pZonaMenor, int pZonaMayor)
	{
		Node<Viaje> actual = listaViajes.darRaiz();
		Node<Viaje> zonaDada = null;
		ArrayList<Integer> zonas = new ArrayList<>();
		boolean encontrado = false;
		ArrayList<String> comparar = new ArrayList<>();
		if(actual.getItem().darTimeType().equals("month")&& actual.getItem().darTime() == pMes)
		{
			while(actual != null && pZonaMenor <= actual.getItem().darIDOrigen() && actual.getItem().darIDOrigen() <= pZonaMayor)
			
			{
				zonas.add(actual.getItem().darIDOrigen());
				actual = actual.getNext();
			} 
			
			while(actual != null && actual.getItem().darIDOrigen() == pZonaDada&& encontrado != true)
			{
				zonaDada = actual;
				encontrado = true;
				actual = actual.getNext();
			}
			
			while (actual != null)
			{
				for(int i = 0; i < zonas.size(); i++)
				{
					if(zonaDada.getItem().darIDDestino()==zonas.get(i))
					
					{
						comparar.add(zonaDada.getItem().darTiempoPromedioViaje() + " de " +pZonaDada + " a " + zonas.get(i));
					} else {
						comparar.add(("No hay viajes" + " de " +pZonaDada + " a " + zonas.get(i)));
					}
					
				}
				
				actual = actual.getNext();
			}
			
			
			
			
		}
		return comparar;
	}
	/**
	 * Generar una gráfica ASCII que muestre el tiempo promedio de los viajes entre una zona origen y una zona 
	 destino para cada hora del día. Cada * en la gráfica corresponde a 1 minuto. Un tiempo promedio se aproxima 
	 a los minutos más cercanos
	 * @param pOrigen
	 * @param pDestino
	 * @return
	 */
	public void tablaAscii(int pOrigen, int pDestino){
		Node<Viaje> actual = listaViajes.darRaiz();
		int viajes = 0;
		String rta="hora sin viajes";

		while(!(actual == null))
		{
			if(actual.getItem().darIDOrigen()==pOrigen && actual.getItem().darIDDestino()==pDestino)
			{String yyy="*";
			double tiempos0=0; int cont0=0;	String s1="";				double tiempos1=0; int cont1=0;
			double tiempos2=0; int cont2=0;					double tiempos3=0; int cont3=0;
			double tiempos4=0; int cont4=0;					double tiempos5=0; int cont5=0;
			double tiempos6=0; int cont6=0;					double tiempos7=0; int cont7=0;
			double tiempos8=0; int cont8=0;					double tiempos9=0; int cont9=0;
			double tiempos10=0; int cont10=0;					double tiempos11=0; int cont11=0;
			double tiempos12=0; int cont12=0;					double tiempos13=0; int cont13=0;
			double tiempos14=0; int cont14=0;					double tiempos15=0; int cont15=0;
			double tiempos16=0; int cont16=0;					double tiempos17=0; int cont17=0;
			double tiempos18=0; int cont18=0;					double tiempos19=0; int cont19=0;
			double tiempos20=0; int cont20=0;					double tiempos21=0; int cont21=0;
			double tiempos22=0; int cont22=0;					double tiempos23=0; int cont23=0;
			double i0=0;double i1=0;double i2=0;double i3=0;double i4=0;double i5=0;double i6=0;double i7=0;double i8=0;
			double i9=0;double i10=0;double i11=0;double i12=0;double i13=0;double i14=0;double i15=0;double i16=0;double i17=0;
			double i18=0;double i19=0;double i20=0;double i21=0;double i22=0;double i23=0;
			{
				if(actual.getItem().darHora()==0){
					
					tiempos0+=actual.getItem().darTiempoPromedioViaje();
					
					cont0++;
				}i0=tiempos0/cont0;if(actual.getItem().darHora()==0){
					tiempos1+=actual.getItem().darTiempoPromedioViaje();
					cont1++;
				}i1=tiempos1/cont1;if(actual.getItem().darHora()==0){
					tiempos2+=actual.getItem().darTiempoPromedioViaje();
					cont2++;
				}i2=tiempos2/cont2;if(actual.getItem().darHora()==0){
					tiempos3+=actual.getItem().darTiempoPromedioViaje();
					cont3++;
				}i3=tiempos3/cont3;if(actual.getItem().darHora()==0){
					tiempos4+=actual.getItem().darTiempoPromedioViaje();
					cont4++;
				}i4=tiempos4/cont4;if(actual.getItem().darHora()==0){
					tiempos5+=actual.getItem().darTiempoPromedioViaje();
					cont5++;
				}i5=tiempos5/cont5;if(actual.getItem().darHora()==0){
					tiempos6+=actual.getItem().darTiempoPromedioViaje();
					cont6++;
				}i6=tiempos6/cont6;if(actual.getItem().darHora()==0){
					tiempos7+=actual.getItem().darTiempoPromedioViaje();
					cont7++;
				}i7=tiempos7/cont7;if(actual.getItem().darHora()==0){
					tiempos8+=actual.getItem().darTiempoPromedioViaje();
					cont8++;
				}i8=tiempos8/cont8;if(actual.getItem().darHora()==0){
					tiempos9+=actual.getItem().darTiempoPromedioViaje();
					cont9++;
				}i9=tiempos9/cont9;if(actual.getItem().darHora()==0){
					tiempos10+=actual.getItem().darTiempoPromedioViaje();
					cont10++;
				}i10=tiempos10/cont10;if(actual.getItem().darHora()==0){
					tiempos11+=actual.getItem().darTiempoPromedioViaje();
					cont11++;
				}i11=tiempos11/cont11;if(actual.getItem().darHora()==0){
					tiempos12+=actual.getItem().darTiempoPromedioViaje();
					cont12++;
				}i12=tiempos12/cont12;if(actual.getItem().darHora()==0){
					tiempos13+=actual.getItem().darTiempoPromedioViaje();
					cont13++;
				}i13=tiempos13/cont13;if(actual.getItem().darHora()==0){
					tiempos14+=actual.getItem().darTiempoPromedioViaje();
					cont14++;
				}i14=tiempos14/cont14;if(actual.getItem().darHora()==0){
					tiempos15+=actual.getItem().darTiempoPromedioViaje();
					cont15++;
				}i15=tiempos15/cont15;if(actual.getItem().darHora()==0){
					tiempos16+=actual.getItem().darTiempoPromedioViaje();
					cont16++;
				}i16=tiempos16/cont16;if(actual.getItem().darHora()==0){
					tiempos17+=actual.getItem().darTiempoPromedioViaje();
					cont17++;
				}i17=tiempos17/cont17;if(actual.getItem().darHora()==0){
					tiempos18+=actual.getItem().darTiempoPromedioViaje();
					cont18++;
				}i18=tiempos18/cont18;if(actual.getItem().darHora()==0){
					tiempos19+=actual.getItem().darTiempoPromedioViaje();
					cont19++;
				}i19=tiempos19/cont19;if(actual.getItem().darHora()==0){
					tiempos20+=actual.getItem().darTiempoPromedioViaje();
					cont20++;
				}i20=tiempos20/cont20;if(actual.getItem().darHora()==0){
					tiempos21+=actual.getItem().darTiempoPromedioViaje();
					cont21++;
				}i21=tiempos21/cont21;if(actual.getItem().darHora()==0){
					tiempos22+=actual.getItem().darTiempoPromedioViaje();
					cont22++;
				}i22=tiempos22/cont22;if(actual.getItem().darHora()==0){
					tiempos23+=actual.getItem().darTiempoPromedioViaje();
					cont23++;
				}i23=tiempos23/cont23;
				actual = actual.getNext();}
			System.out.println("Aproximación en minutos de viajes entre zona origen y zona destino"+
					"\nTrimestre X del 2018 detallado por cada hora del día"+"\nZona Origen:"+pOrigen+"\nZona Destino:"
					+pDestino+"\nHora|  # de minutos"+"\n00  |");
			if(cont0==0){yyy=rta;}if(cont1==0){yyy=rta;}if(cont2==0){yyy=rta;}if(cont3==0){yyy=rta;}if(cont4==0){yyy=rta;}if(cont5==0){yyy=rta;}if(cont6==0){yyy=rta;}if(cont7==0){yyy=rta;}
			if(cont8==0){yyy=rta;}if(cont9==0){yyy=rta;}if(cont10==0){yyy=rta;}if(cont11==0){yyy=rta;}if(cont12==0){yyy=rta;}if(cont13==0){yyy=rta;}if(cont14==0){yyy=rta;}if(cont15==0){yyy=rta;}
			if(cont16==0){yyy=rta;}if(cont17==0){yyy=rta;}if(cont18==0){yyy=rta;}if(cont19==0){yyy=rta;}if(cont20==0){yyy=rta;}if(cont21==0){yyy=rta;}if(cont22==0){yyy=rta;}if(cont23==0){yyy=rta;}
			for(int i=0; i < i0; i++) 
				System.out.println(yyy);
			System.out.println("\n01  |");	for(int i=0; i < i1; i++) 
				System.out.println(yyy);
			System.out.println("\n02  |");for(int i=0; i < i2; i++) 
				System.out.println(yyy);
			System.out.println("\n03  |");for(int i=0; i < i3; i++) 
				System.out.println(yyy);
			System.out.println("\n04  |");for(int i=0; i < i4; i++) 
				System.out.println(yyy);
			System.out.println("\n05  |");for(int i=0; i < i5; i++) 
				System.out.println(yyy);
			System.out.println("\n06  |");for(int i=0; i < i6; i++) 
				System.out.println(yyy);
			System.out.println("\n07  |");for(int i=0; i < i7; i++) 
				System.out.println(yyy);
			System.out.println("\n08  |");for(int i=0; i < i8; i++) 
				System.out.println(yyy);
			System.out.println("\n09  |");for(int i=0; i < i9; i++) 
				System.out.println(yyy);
			System.out.println("\n010  |");for(int i=0; i < i10; i++) 
				System.out.println(yyy);
			System.out.println("\n011  |");for(int i=0; i < i11; i++) 
				System.out.println(yyy);
			System.out.println("\n012  |");for(int i=0; i < i12; i++) 
				System.out.println(yyy);
			System.out.println("\n013  |");for(int i=0; i < i13; i++) 
				System.out.println(yyy);
			System.out.println("\n014  |");for(int i=0; i < i14; i++) 
				System.out.println(yyy);
			System.out.println("\n015  |");for(int i=0; i < i15; i++) 
				System.out.println(yyy);
			System.out.println("\n016  |");for(int i=0; i < i16; i++) 
				System.out.println(yyy);
			System.out.println("\n017  |");for(int i=0; i < i17; i++) 
				System.out.println(yyy);
			System.out.println("\n018  |");for(int i=0; i < i18; i++) 
				System.out.println(yyy);
			System.out.println("\n019  |");for(int i=0; i < i19; i++) 
				System.out.println(yyy);
			System.out.println("\n020  |");for(int i=0; i < i20; i++) 
				System.out.println(yyy);
			System.out.println("\n021  |");for(int i=0; i < i21; i++) 
				System.out.println(yyy);
			System.out.println("\n022  |");for(int i=0; i < i22; i++) 
				System.out.println(yyy);
			System.out.println("\n023  |");for(int i=0; i < i23; i++) 
				System.out.println(yyy);
			}


		}

	}

	/**
	 * Consultar el tiempo promedio de viaje y su desviación estándar de los viajes entre una zona
	//	de origen y una zona destino para un mes dado. Reportar el caso especial en que No exista información al respecto.
	 * @param pOrigen
	 * @param pDestino
	 * @param pMes
	 */
	public void UnoA(int pOrigen, int pDestino, int pMes){
		Node<Viaje> actual = listaViajes.darRaiz();

		while(!(actual == null))
		{
			if(actual.getItem().darIDOrigen()==pOrigen && actual.getItem().darIDDestino()==pDestino && actual.getItem().darTime()==pMes)
			{
				System.out.println(actual.getItem().darTiempoPromedioViaje()+actual.getItem().darDerivacionEstandarViaje());
				if(actual.getItem().darTiempoPromedioViaje()==0 && actual.getItem().darDerivacionEstandarViaje()==0){
					System.out.println("no existe info. al respecto");
				}}

			actual = actual.getNext();
		}

	}
	public void UnoB(int pOrigen, int pDestino, int pDia){
		Node<Viaje> actual = listaViajes.darRaiz();

		while(!(actual == null))
		{
			if(actual.getItem().darIDOrigen()==pOrigen && actual.getItem().darIDDestino()==pDestino && actual.getItem().darTime()==pDia)
			{
				System.out.println(actual.getItem().darTiempoPromedioViaje()+actual.getItem().darDerivacionEstandarViaje());
				if(actual.getItem().darTiempoPromedioViaje()==0 && actual.getItem().darDerivacionEstandarViaje()==0){
					System.out.println("no existe info. al respecto");
				}
			}
			actual = actual.getNext();
		}

	}
	//	Consultar los viajes entre una zona de origen y una zona destino en una franja horaria (hora inicial – hora final) dada. La franja horaria se define con horas enteras. Mostrar los viajes indicando el tiempo promedio de viaje y su desviación estándar para cada hora entera iniciando en la hora inicial y terminando en la hora final.
	public void UnoC(int horaInicio, int horaFin){
		Node<Viaje> actual = listaViajes.darRaiz();

		while(!(actual == null))
		{
			if(actual.getItem().darIDOrigen()==horaInicio && actual.getItem().darIDDestino()==horaFin )
			{
				System.out.println(actual.getItem().darTiempoPromedioViaje()+actual.getItem().darDerivacionEstandarViaje());
				if(actual.getItem().darTiempoPromedioViaje()==0 && actual.getItem().darDerivacionEstandarViaje()==0){
					System.out.println("no existe info. al respecto");
				}
			}
			actual = actual.getNext();
		}

	}}






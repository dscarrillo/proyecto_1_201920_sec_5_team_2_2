package controller;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;
import model.data_structures.*;


public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;
	
	private final static String ruta1 = "data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv";
	private final static String ruta2 = "data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv";
	private final static String ruta3 = "data/bogota-cadastral-2018-3-All-MonthlyAggregate.csv";
	private final static String ruta4 = "data/bogota-cadastral-2018-4-All-MonthlyAggregate.csv";

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		Integer dato = 0;
		Integer dato2 = 0;
		Integer dato3 = 0;
		int contMes = 0;
		int contDia = 0;
		int contHora = 0;
		String respuesta = "";
		ListaEnlazada<Viaje> auxiliar = new ListaEnlazada();
		ArrayList aux = new ArrayList();
		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nSeleccione un trimestre:");
				System.out.println("\n1. Primer Trimestre.");
				System.out.println("\n2. Segundo Trimestre.");
				System.out.println("\n3. Tercer Trimestre.");
				System.out.println("\n4. Cuarto Trimestre.");
				int opt = lector.nextInt();
				switch(opt)
				{
				case 1:
					System.out.println("-------------------\nCargando viajes...");
					
					contMes=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv", "month");	
					System.out.println("Archivo 1 cargado");
					contDia=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-1-WeeklyAggregate.csv", "dow");
					System.out.println("Archivo 2 cargado");
					contHora=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-1-All-HourlyAggregate.csv", "hod");
					System.out.println("Archivo 3 cargado");
					System.out.println("La cantidad de lineas de leidas en el archivo mes fueron: "+contMes+"\nLa cantidad de lineas de leidas en el archivo dia fueron: " + contDia + "\nLa cantidad de lineas de leidas en el archivo hora fueron: "+ contHora+ "\nEl ID de la zona con menor ID es: " + modelo.menorID()+"\nEl ID de la zona con mayor ID es: " + modelo.mayorID());
					
					break;
				case 2:
					System.out.println("-------------------\nCargando viajes...");
					contMes=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv", "month");	
					contDia=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-2-WeeklyAggregate.csv", "dow");
					contHora=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-2-All-HourlyAggregate.csv", "hod");
					System.out.println("La cantidad de lineas de leidas en el archivo mes fueron: "+contMes+"\nLa cantidad de lineas de leidas en el archivo dia fueron: " + contDia + "\nLa cantidad de lineas de leidas en el archivo hora fueron: "+ contHora+ "\nEl ID de la zona con menor ID es: " + modelo.menorID()+"\nEl ID de la zona con mayor ID es: " + modelo.mayorID());
					break;
				case 3:
					System.out.println("-------------------\nCargando viajes...");
					contMes=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-3-All-MonthlyAggregate.csv", "month");	
					contDia=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-3-WeeklyAggregate.csv", "dow");
					contHora=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-3-All-HourlyAggregate.csv", "hod");
					System.out.println("La cantidad de lineas de leidas en el archivo mes fueron: "+contMes+"\nLa cantidad de lineas de leidas en el archivo dia fueron: " + contDia + "\nLa cantidad de lineas de leidas en el archivo hora fueron: "+ contHora+ "\nEl ID de la zona con menor ID es: " + modelo.menorID()+"\nEl ID de la zona con mayor ID es: " + modelo.mayorID());
					break;
				case 4:
					System.out.println("-------------------\nCargando viajes...");
					contMes=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-4-All-MonthlyAggregate.csv", "month");	
					contDia=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-4-WeeklyAggregate.csv", "dow");
					contHora=modelo.cargarViajesALinkedList("data/bogota-cadastral-2018-4-All-HourlyAggregate.csv", "hod");
					System.out.println("La cantidad de lineas de leidas en el archivo mes fueron: "+contMes+"\nLa cantidad de lineas de leidas en el archivo dia fueron: " + contDia + "\nLa cantidad de lineas de leidas en el archivo hora fueron: "+ contHora+ "\nEl ID de la zona con menor ID es: " + modelo.menorID()+"\nEl ID de la zona con mayor ID es: " + modelo.mayorID());
					break;
				}
				
				
				break;

			case 2:
				System.out.println("--------- \nDar mes a consultar: ");
				dato = Integer.parseInt(lector.next());
				if(dato == 01 || dato == 02 || dato == 03 || dato == 04 || dato == 05 || dato == 06)
				{
					System.out.println("--------- \nDar zona de origen a consultar: ");
					dato2 = Integer.parseInt(lector.next());
					auxiliar = modelo.consultarViajes(dato, dato2);
					if(auxiliar != null && auxiliar.getTamanio() != 0)
					{
						Node<Viaje> actual = auxiliar.darRaiz();
						
						if(actual.getItem() != null)
						{
							System.out.println("--------- \nZona de origen: " + actual.getItem().darIDOrigen() + ", Zona destino: " + actual.getItem().darIDDestino()+ ", Tiempo promedio: " + actual.getItem().darTiempoPromedioViaje()+ ", Desviaci�n Est�ndar: " + actual.getItem().darDerivacionEstandarViaje());
						} else {
							actual.getNext();
						}
										
					} else {
						System.out.println("--------- \nEn el mes " + dato + " no hubieron viajes con zona de origen " + dato2);
					}
				} else {
					System.out.println("Mes inv�lido, el mes debe ser ingresado con 2 digitos, ejemplo: Enero = 01 y debe estar en el primer semestre del a�o");
				}				
				 				
				break;		
				
			case 3:
				System.out.println("--------- \nDar mes a consultar: ");
				dato = Integer.parseInt(lector.next());
				if(dato == 01 || dato == 02 || dato == 03 || dato == 04 || dato == 05 || dato == 06)
				{
					System.out.println("--------- \nDar zona de origen a consultar: ");
					dato2 = Integer.parseInt(lector.next());
					aux = modelo.darEstadisticas(dato, dato2);
					if(aux != null){
						System.out.println("--------- \nEstad�sticas: \nTotal de viajes en el semestre: "+ aux.get(0)+"\nTotal de viajes en el mes de consulta: "+aux.get(1)+" , porcentaje con respecto al total de viajes: "+ aux.get(2) + "%\nTotal de viajes en el mes y  id de origen de consulta: " + aux.get(3)+ " , porcentaje con respecto al total de viajes del mes de consulta: "+aux.get(4)+"%");
					} else {
						System.out.println();
					}
				} else {
					System.out.println("Mes inv�lido, el mes debe ser ingresado con 2 digitos, ejemplo: Enero = 01 y debe estar en el primer semestre del a�o");
				}	
				break;
			case 4: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
